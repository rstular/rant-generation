# rant-generation
Utilizing machine learning to generate text, that will replicate rants from [devrant](www.devrant.com) as closely as possible, using [TensorFlow](tensorflow.org) framework.
Please note that some of the code is not mine, see license for details.

### Installing dependencies
Install all dependencies by running `pip3 install -r requirements.txt`
### Downloading rants
Rants are downloaded using devRant API ([unofficial documentation](https://devrant-docs.github.io/)). To download the dataset, run `python download.py`. Default download location is `./dataset/rants.txt`. If you want to change settings (e.g. delay between requests or dataset file name, just edit the file `download.py`.
Hopefully the code is documented enough for you to understand.
### Training the model
Usage of `tf_wrapper.py`:

      usage: tf_wrapper.py train [-h] --checkpoint-path CHECKPOINT_PATH 
                                      --text-path TEXT_PATH
                                      [--restore [RESTORE]]
                                      [--seq-len SEQ_LEN]
                                      [--embedding-size EMBEDDING_SIZE]
                                      [--rnn-size RNN_SIZE] 
                                      [--num-layers NUM_LAYERS]
                                      [--drop-rate DROP_RATE]
                                      [--learning-rate LEARNING_RATE]
                                      [--clip-norm CLIP_NORM] 
                                      [--batch-size BATCH_SIZE]
                                      [--num-epochs NUM_EPOCHS]
                                      [--log-path LOG_PATH]
    
    optional arguments:
      -h, --help            show this help message and exit
      --checkpoint-path CHECKPOINT_PATH
                            path to save or load model checkpoints
      --text-path TEXT_PATH
                            path of text file for training
      --restore [RESTORE]   whether to restore from checkpoint_path or from
                            another path if specified
      --seq-len SEQ_LEN     sequence length of inputs and outputs (default: 64)
      --embedding-size EMBEDDING_SIZE
                            character embedding size (default: 32)
      --rnn-size RNN_SIZE   size of rnn cell (default: 128)
      --num-layers NUM_LAYERS
                            number of rnn layers (default: 2)
      --drop-rate DROP_RATE
                            dropout rate for rnn layers (default: 0.0)
      --learning-rate LEARNING_RATE
                            learning rate (default: 0.001)
      --clip-norm CLIP_NORM
                            max norm to clip gradient (default: 5.0)
      --batch-size BATCH_SIZE
                            training batch size (default: 64)
      --num-epochs NUM_EPOCHS
                            number of epochs for training (default: 32)
      --log-path LOG_PATH   path of log file (default: main.log)

Example:

    python tf_wrapper.py train --checkpoint=checkpoint.cp --text=dataset/rants.txt
### Generating AI-rants
Usage of the *generate* command:

    usage: tf_wrapper.py generate [-h] --checkpoint-path CHECKPOINT_PATH
                                         (--text-path TEXT_PATH | --seed SEED)
                                         [--length LENGTH] [--top-n TOP_N]
                                         [--log-path LOG_PATH]
    
    optional arguments:
      -h, --help            show this help message and exit
      --checkpoint-path CHECKPOINT_PATH
                            path to load model checkpoints
      --text-path TEXT_PATH
                            path of text file to generate seed
      --seed SEED           seed character sequence
      --length LENGTH       length of character sequence to generate (default:
                            1024)
      --top-n TOP_N         number of top choices to sample (default: 3)
      --log-path LOG_PATH   path of log file (default: main.log)
*Please note that **seed** indicates the beginning of the generated sequence (which is an username in this case)*
Example:

    python tf_model.py generate --checkpoint=checkpoint.cp --seed="rstular"

I will post some examples of generated text as soon as the model finishes training, it takes quite a lot of time on my poor PC

If you want to get better results, try to play with input arguments and let me know if you stumble across something better :-D.

