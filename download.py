import requests
import json
import time
from argparse import ArgumentParser
from string import Template

parser = ArgumentParser(description="Download rants from devRant via devRant API")
parser.add_argument("--delay", help="Delay between requests in seconds (default=1)", type=int, default=1)
parser.add_argument("--filename", default="dataset/rants.txt", help="Destination file for rants (default=dataset/rants.txt)")
parser.add_argument("--numrants", default=40000, type=int, help="Number of rants to download (aproximation, default=40000)")
args = parser.parse_args()

# See https://devrant-docs.github.io/ for more info
API_TEMPLATE_STRING_ALL = Template("https://devrant.com/api/devrant/rants?app=3&sort=top&range=all&limit=20&skip=${skip}")

# String template for saving rants (currently username: rant_text)
LINE_TEMPLATE = Template("${username}: ${text}\n")

# Output file name
FILE_NAME = args.filename
# Delay between requests (default 1 second), because we don't want to DoS devRant :-D
DELAY = args.delay

# Number of rants to download
NUM_RANTS = args.numrants

# We want to catch KeyboardInterrupt to close the file before the script exits
try:
    # Open the file
    rant_file = open(FILE_NAME, "a")
    # Some serious math going on right there (JK, since one requests returns 20 rants, the number of request needed is NUMBER_OF_RANTS_WE_WANT / 20
    # But since I didn't want to bother with floats (I just cut of the decimals), we might get a bit smaller number of rants than expected
    for i in range(0,int(NUM_RANTS/20)):
        # Printing progress (since non-verbose waiting is painful), the calculation might not be 100% correct
        print("[*] completed: " + str(round(i*100/(NUM_RANTS/20), 2)) + "% (i=" + str(i) + ")")
        # This is dirty AF, basically we insert number of rants to skip (see API documentation for more info, link on line 6), which is i*20 (since one request returns 20 rants)
        # Then we convert the response to string and then to JSON
        allrants_json = json.loads(requests.get(API_TEMPLATE_STRING_ALL.substitute(skip=str(i*20))).text)
        # Here we loop through all returned rants
        for rant in allrants_json["rants"]:
            # And write everything to file
            rant_text = LINE_TEMPLATE.substitute(username=rant["user_username"], text=rant["text"].replace("\n"," "))
            rant_file.write(rant_text)
        # Sleep, to prevent DoS ;-)
        time.sleep(DELAY)
except KeyboardInterrupt:
    # If the user interrupts, close the file
    rant_file.close()
